# TeX für Windows

##Installation Cygwin und Git 

Download Cygwin: 
https://www.cygwin.com/

`Install from Internet` auswählen und auf `weiter` klicken.
Bei der Installation die benötigten Pakete hinzufügen:
* [x] GIT
* [x] VIM

Die Abhängigkeiten werden automatisch hinzugefügt und müssen nur noch bestätigt werden.

Die Cygwin Konsole nach der Installation öffnen und zu dem Projektverzeichniss navigieren. 
  
Mit dem Befehl `git clone https://[username]@bitbucket.org/naoroboter/netzwerkmanagement.git` das Repository clonen.   
Der in Bitbucket hinterlegte User muss in GIT noch bekannt gemacht werden:
* `git config --global user.email "you@example.com"`
* `git config --global user.name "your name"`

Der default editor für die commit message kann mit der folgenden Anweisung geändert werden. Anstatt vim kann hier auch der Pfad zu einem belibigen editor angegen werden.
* `git config --global core.editor vim`


Die folgenden Kommandos werden in der Cygwin-Konsole in dem geclonten Branch ausgeführt:

Wie sehe ich geänderte Dateien?
* `git status`

Wie kann ich Änderungen einer Datei sehen?
* `git diff dir/filename`

Geänderte Dateien hinzufügen.
* `git add dir/filename`

Dateien zum lokalen Repro hinzufügen. In dem geöffneten Fenster die commit message eingeben.
* `git commit` 

Die Änderungen zum origin übertragen.
* `git push`   

## Installation Latex

Als Interpreter der Tex-Dokumente für Windows das Paket MikTEX 2.9 installieren.
* `http://www.miktex.org/2.9/setup`

Zum editieren und compilieren wird noch ein weiteres Programm (z. B. Texmaker) benötigt.
* `http://www.xm1math.net/texmaker/download.html#windows`

<<<<<<< HEAD
##Arbeiten mit Texmaker
Texmaker öffnen und die Datei Project/master/beleg/main.tex im Editor öffnen. Auf die Schaltfläche `schnelles übersetzen` 
klicken und die benötigten Packete nach installieren. Das ist notwendig, da die mixtex Installation nur ein Minimum an 
Paketen enthält, aber wir durch die Deklaration `\usepackage irgendwas` weitere Plugins benötigen.

Nach Installation der benötigten Pakete erscheint in dem rechten Fenster die PDF-Vorschau.
Für die Angabe der Quellen habe ich zwei Dateine angelegt.
* Literaturquellen: `quellenBuch.bib` 
* Internetquellen : `quellenWeb.bib`  

Die Dateien sind jeweils mit einem Beispieldatensatz gefüllt, dieser ist selbsterklärend. Falls ihr 
Quellen in irgendeiner Form benutzt bitte die Quelle in die jeweilige Datei übernehmen.
Um das Literatur- oder Webverzeichnis zu aktualisieren, muss das Dokument zweimal kompiliert werden (schnelles übersetzen).
Auf der Kommandozeile den Befehl `bibtex Refs.aux` für Literaturquellen und für Internetquellen den Befehl 
`bibtex Urls.aux` im Ordner /master/beleg ausführen. Anschließend das Dokument noch ein weiteres mal übersetzen und die 
Quellenverzeichnisse werden angelegt.

Die Datei `main.tex` ist sehr ausführlich dokumentiert. Ein Kommentar beginnt immer mit dem %-Zeichen.
Bei der Arbeit mit Latex Dokumenten darauf achten, keine Sonderzeichen zu benutzen, diese müssen meist mit `\` escaped werden.
Anführungszeichen unten und oben werden im Text mit `\su Text in Anführungszeichen \so` erstellt.

Hier noch einige Links zum Arbeiten mit Latex Dokumenten:
* http://wissrech.ins.uni-bonn.de/people/feuersaenger/MeineKurzReferenz.pdf
* http://latex.mschroeder.net/
* http://www.weinelt.de/latex/ (Befehlsreferenz)

## FAQ
Beim compilieren erhalte ich die Fehlermeldung `! I can't write on file main.pdf'.` 
* Die PDF-Datei ist in einem anderen Programm geöffnet(z. B. Adobe) und kann nicht überschrieben werden.
* Die Datei ist im Dateimanager von Windows selektiert.



  
  
  










=======
pdf
>>>>>>> 1b4ebe95670122603b7d0416ce47241aca9dcbde











